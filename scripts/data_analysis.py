import pandas as pd

def statdesc(data):
    """
    Compute basic statistics about the given dataframe.
    Parameters:
    - data (DataFrame): The input dataframe containing at least 'year' and 'country_code' columns.
    Returns:
    Tuple: A tuple containing the minimum year, maximum year, and number of unique countries in the dataframe.
    """
    min_year = data['year'].min()
    max_year = data['year'].max()
    num_countries = data['country_code'].nunique()
    print(f'The dataframe covers the period from {min_year} to {max_year} and includes {num_countries} countries.')
    return min_year, max_year, num_countries

def get_miss_values(data, col):
    """
    Count the number of missing values in a specific column of the dataframe.
    Parameters:
    - data (DataFrame): The input dataframe.
    - col (str): The column for which missing values are counted.
    Returns:
    int: The number of missing values in the specified column.
    """
    return data[col].isnull().sum()

def desc_missing(data, col):
    """
    Provide a description of missing values in the dataframe for a specific column.
    Parameters:
    - data (DataFrame): The input dataframe.
    - col (str): The column for which missing values are described.
    Prints:
    - Information about the number of non-missing and missing values.
    - The percentage of missing values in the dataset.
    """
    min_year, max_year, num_countries = statdesc(data)
    missing_values = get_miss_values(data, col)
    non_missing_values = data[col].count()
    ratio_missing_to_total = missing_values / ((max_year - min_year) * num_countries)
    print(f'There are {non_missing_values} observations and {missing_values} missing values.')
    print(f'The percentage of missing values in the dataset is {ratio_missing_to_total:.2%}.')

def non_nan_years(data, first=True):
    """
    Returns the first or last non-NaN years for each column in the DataFrame.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - first (bool): If True, returns the first non-NaN year; otherwise, the last non-NaN year.
    Returns:
    Series: A series containing the first or last non-NaN years for each column.
    """
    if first:
        return data.apply(lambda x: x.first_valid_index())
    else:
        return data.apply(lambda x: x.last_valid_index())

def calculate_total(values):
    """
    Calculates the total sum of values.
    Parameters:
    - values: An iterable containing numerical values.
    Returns:
    int: The total sum of the provided values.
    """
    return values.sum()

def add_miss_values(data, first=True):
    """
    Adds up missing values before or after the series.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - first (bool): If True, adds missing values before the series start; otherwise, after the series end.
    Returns:
    int: The total count of missing values for the specified timing.
    """
    if first:
        first_non_nan_years = non_nan_years(data, first=True)
        first_missing_values = first_non_nan_years - 1990
        return calculate_total(first_missing_values)
    else:
        last_non_nan_years = non_nan_years(data, first=False)
        last_missing_values = 2021 - last_non_nan_years
        return calculate_total(last_missing_values)

def ratio_na(data, subtotal):
    """
    Calculates the ratio of missing values to the total in the dataset.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - subtotal (int): The count of missing values for a specific calculation.
    Returns:
    float: The calculated ratio of missing values to the total in the dataset.
    """
    return round(subtotal * 100 / data.isna().sum().sum(), 1)
    
def early_missing_values(data):
    """
    Calculates and displays missing values before the series start.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    """
    total_first_missing_values = add_miss_values(data, True)
    ratio_first_to_total = ratio_na(data, total_first_missing_values)
    print("Number of missing values before the series start:", total_first_missing_values)
    print("Ratio to total missing values in the dataset:", ratio_first_to_total, "%")

def final_missing_values(data):
    """
    Calculates and displays missing values after the series end.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    """
    total_last_missing_values = add_miss_values(data, False)
    ratio_last_to_total = ratio_na(data, total_last_missing_values)
    print("Number of missing values after the series end:", total_last_missing_values)
    print("Ratio to total missing values in the dataset:", ratio_last_to_total, "%")

def count_miss_values(data, early=True):
    """
    Calculates missing values based on the specified timing.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - early (bool): If True, calculates missing values for the early period; otherwise, for the final period.
    Returns:
    int: The count of missing values based on the specified timing.
    """
    if early:
        return early_missing_values(data)
    else:
        return final_missing_values(data)

def interior_missing_values(data, col):
    """
    Calculates the interior missing values for a specific column.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - col (str): The column for which interior missing values are calculated.
    Returns:
    int: The count of interior missing values for the specified column.
    """
    return get_miss_values(data, col) - count_miss_values(data, early=True) - count_miss_values(data, early=False)

def count_interior_nan(data):
    """
    Counts interior NaN values for each country.
    Parameters:
    - data (DataFrame): The input dataframe containing debt data.
    Returns:
    Series: A series containing the count of interior NaN values for each country.
    """
    nan_counts = data.isna().sum()
    first_missing_values = non_nan_years(data, first=True) - 1990
    last_missing_values = 2021 - non_nan_years(data, first=False)
    real_missing_values = nan_counts - (first_missing_values + last_missing_values)
    return real_missing_values

def big_nan_countries(data, seuil):
    """
    Identifies countries with more consecutive NaN values than the specified threshold.
    Parameters:
    - data (DataFrame): The input dataframe containing debt data.
    - seuil (int): The threshold for the number of consecutive NaN values.
    Returns:
    Tuple: A tuple containing the count of countries exceeding the threshold and a list of those countries.
    """
    real_missing_values = count_interior_nan(data)
    more_than_one_real_nan = (real_missing_values > seuil).sum()
    countries_with_more_than_one_real_nan = real_missing_values[real_missing_values > seuil].index.tolist()
    return more_than_one_real_nan, countries_with_more_than_one_real_nan 

def show_interior_nan_series(data, seuil):
    """
    Displays series with interior NaN values for countries exceeding the threshold.
    Parameters:
    - data (DataFrame): The input dataframe containing debt data.
    - seuil (int): The threshold for the number of interior NaN values.
    Prints:
    - Information about countries exceeding the threshold and displays their data with interior NaN values.
    """
    more_than_one_real_nan, countries_with_more_than_one_real_nan = big_nan_countries(data, seuil)
    print(more_than_one_real_nan, f"pays ont au moins {seuil+1} valeurs manquantes :", 
          ", ".join(countries_with_more_than_one_real_nan),".")
    data_transposed = data.transpose()
    selected_countries_data = data_transposed[data_transposed.index.isin(countries_with_more_than_one_real_nan)]
    selected_countries_data = selected_countries_data.transpose()
    print(selected_countries_data.to_string(index=True))

def non_nan_window(data):
    """
    Identify the first and last non-NaN years for each country in the dataframe.
    Parameters:
    - data (DataFrame): The input dataframe containing debt data.
    Returns:
    DataFrame: A dataframe with columns 'Country Code', 'Debt first non-NaN year', and 'Debt last non-NaN year'.
    """
    first_non_nan_years = non_nan_years(data, first=True)
    last_non_nan_years = non_nan_years(data, first=False)
    data_dates = pd.DataFrame({
        'Country Code': first_non_nan_years.index,
        'Debt first non-NaN year': first_non_nan_years.values,
        'Debt last non-NaN year': last_non_nan_years.values
    })
    return data_dates

def interpolation(data, ref_col, target_col, seuil):
    """
    Interpolates missing values in the target column based on neighboring values.
    Parameters:
    - data (DataFrame): Input DataFrame.
    - ref_col (str): Reference column for grouping data.
    - target_col (str): Column with missing values to be interpolated.
    - seuil (int): Threshold for identifying countries with more than one consecutive NaN in the target column.
    Returns:
    - DataFrame: New DataFrame with interpolated missing values.
    """
    countries_with_more_than_one_real_nan = big_nan_countries(data, seuil)
    data_dates = non_nan_window(data)
    full_data = data.copy()
    for index, row in data_dates.iterrows():
        country_code = row['Country Code']
        start_year = row['Debt first non-NaN year']
        end_year = row['Debt last non-NaN year']
        if country_code not in countries_with_more_than_one_real_nan:
            selected_rows = full_data[(full_data[ref_col] == country_code) & (full_data['year'] >= start_year) & (full_data['year'] <= end_year)]
            for i in range(1, len(selected_rows) - 1):
                if pd.isna(selected_rows.iloc[i][col]):
                    avg_value = (selected_rows.iloc[i - 1][col] + selected_rows.iloc[i + 1][col]) / 2
                    full_data.loc[(full_data[ref_col] == country_code) & (full_data['year'] == selected_rows.iloc[i]['year']), col] = avg_value
    return full_data

def fill_missing_values_with_avg(data, dates, ref_col, start_year_col, end_year_col, y_col):
    """
    Fills isolated NaN values in the specified column ('y_col') by replacing them with the average of adjacent values.
    The selection of missing values is based on the start and end years specified for each country in the reference column.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - ref_col (str): The reference column used for grouping countries.
    - start_year_col (str): The column containing the starting year for each country.
    - end_year_col (str): The column containing the ending year for each country.
    - y_col (str): The column containing the values to be filled.
    Returns:
    DataFrame: The modified dataframe with filled missing values.
    """
    nan_counts = data.isna().sum()
    real_missing_values = nan_counts - (data[start_year_col] - 1990) - (2022 - data[end_year_col])
    countries_with_more_than_one_real_nan = real_missing_values[real_missing_values > 1].index.tolist()
    filled_data = data.copy()
    for index, row in dates.iterrows():
        country_code = row['Country Code']
        start_year = row[start_year_col]
        end_year = row[end_year_col]
        if country_code not in countries_with_more_than_one_real_nan:
            selected_rows = filled_data[(filled_data[ref_col] == country_code) & (filled_data['year'] >= start_year) & (filled_data['year'] <= end_year)]
            for i in range(1, len(selected_rows) - 1):
                if pd.isna(selected_rows.iloc[i][y_col]):
                    avg_value = (selected_rows.iloc[i - 1][y_col] + selected_rows.iloc[i + 1][y_col]) / 2
                    filled_data.loc[(filled_data[ref_col] == country_code) & (filled_data['year'] == selected_rows.iloc[i]['year']), y_col] = avg_value
    return filled_data

def adjust_column_values(data, ref_col, target_value, start_year, end_year, y_col):
    """
    Adjusts the values in a specific column ('y_col') for a given subset of data defined by the reference column ('ref_col'),
    a target column value ('target_column'), and a range of years ('start_year' to 'end_year').
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - ref_col (str): The reference column used for filtering the data.
    - target_column (str): The target value in the reference column for adjustment.
    - start_year (int): The starting year of the adjustment range.
    - end_year (int): The ending year of the adjustment range.
    - y_col (str): The column whose values will be adjusted.
    Returns:
    None: The function modifies the input dataframe in place.
    """
    subset_data = data[(data[ref_col] == target_value) & (data['year'].isin([start_year, end_year]))]
    delta_column = subset_data.groupby(ref_col)[y_col].diff().iloc[-1] / (end_year - start_year)
    for year in range(start_year + 1, end_year):
        mask_year = (data[ref_col] == target_value) & (data['year'] == year)
        data.loc[mask_year, y_col] = (
            subset_data.loc[subset_data['year'] == start_year, y_col].values[0]
            + delta_column * (year - start_year)
        )

def compute_growth(data, shift):
    """
    Computes the growth rates shifted by a specified number of years for a given data type.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - shift (int): The number of years to shift for computing growth rates.
    Returns:
    DataFrame: The modified dataframe with a new column for the computed growth rates.
    """
    data[f'{str(shift-1)}_years_population_growth'] = (
    data.groupby('country_code')['population']
    .transform(lambda x: x.shift(1-shift) / x)
) - 1
    return data

def pays_manquants(data, data_name, col, ref):
    """
    Finds countries that are in the 'data' dataset but not in the 'ref' dataset.
    Parameters:
    - data (DataFrame): The main dataset.
    - data_name (str): A description of the dataset.
    - col (str): The column containing country codes in both datasets.
    - ref (set): The reference set of country codes (intersection of datasets).
    Returns:
    set: A set of countries missing from the 'ref' dataset.
    """
    print(f"Number of countries missing from {data_name} data: {len(ref - set(data[col]))}")
    return ref - set(data[col])

def display_missing_country_names(missing_country_codes, country_data):
    """
    Displays the names of countries corresponding to the missing country codes.
    Parameters:
    - missing_country_codes (set): Set of country codes that are missing.
    - country_data (DataFrame): DataFrame with columns 'country_code' and 'country_name'.
    """
    for country_code in missing_country_codes:
        country_row = country_data[country_data['country_code'] == country_code]
        if not country_row.empty:
            country_name = country_row['country_name'].values[0]
            print(f"{country_name}")
        else:
            print(f"{country_code} (country name not found in database)")