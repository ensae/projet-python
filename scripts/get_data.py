import bs4
import json
import numpy as np
import pandas as pd
import requests
import zipfile
from urllib import request
from io import BytesIO

def get_raw_ISO(url):
    """
    Retrieves the raw HTML data for ISO country codes from a specified URL.
    Parameters:
    - url (str): The URL of the webpage containing ISO country code information.
    Returns:
    ResultSet: A BeautifulSoup ResultSet containing the rows of the ISO country codes table.
    """
    request_text = request.urlopen(url).read()
    page = bs4.BeautifulSoup(request_text, "lxml")
    table_iso_country_codes = page.find('table', {'class' : 'wikitable sortable'})
    table_iso_country_codes_body = table_iso_country_codes.find('tbody')
    rows = table_iso_country_codes_body.find_all('tr')
    return rows

def get_raw_data(api_url):
    """
    Calls a public API to retrieve data in DataFrame format.
    Parameters:
    - api_url (str): The URL of the API.
    Returns:
    DataFrame: The data in DataFrame format.
    """
    response = requests.get(api_url)
    data = response.json()
    return pd.DataFrame(data)

def check_api_availability(api_url):
    try:
        response = requests.get(api_url, timeout = 15)
        response.raise_for_status()  # # Lèvera une exception pour les erreurs HTTP
        return True
    except requests.exceptions.RequestException as api_exception:
        print(f"L'API est indisponible : {api_exception}")
        return False

def load_local_data(file_path):
    """
    Loads and preprocesses local HDI data from a specified file path.
    Parameters:
    - file_path (str): The path to the local file containing HDI data.
    Returns:
    DataFrame: The preprocessed HDI data in DataFrame format.
    """
    try:
        local_data = pd.read_csv(file_path)
        local_data = local_data.iloc[:, :37]
        columns_to_drop = local_data.columns[1:5]
        hdi_data = local_data.drop(columns=columns_to_drop, axis=1)
        hdi_data = hdi_data[~hdi_data['iso3'].str.startswith('ZZ')]
        columns_to_rename = hdi_data.columns[1:33]
        hdi_data.rename(columns={col: col.replace('hdi_', '') for col in columns_to_rename}, inplace=True)
        hdi_data = hdi_data.rename(columns={'iso3': 'country_code'})
        hdi_data = pd.melt(hdi_data, id_vars=['country_code'], var_name='year', value_name='HDI')
        hdi_data['year'] = pd.to_numeric(hdi_data['year'], errors='coerce').astype('Int64')
        hdi_data['HDI'] = pd.to_numeric(hdi_data['HDI'], errors='coerce')
        hdi_data['HDI'].replace([np.inf, -np.inf], np.nan, inplace=True)
        hdi_data = hdi_data.reset_index(drop=True)
        return hdi_data
    except Exception as e:
        print(f"Error loading local data: {e}")

def get_raw_pop(url):
    """
    Retrieves World Bank population data from a specified URL.
    Parameters:
    - url (str): The URL of the World Bank population data in ZIP format.
    Returns:
    DataFrame: The raw World Bank population data in DataFrame format.
    """
    response = requests.get(url)
    zip_file = zipfile.ZipFile(BytesIO(response.content))
    csv_filename = "API_SP.POP.TOTL_DS2_en_csv_v2_6224560.csv"
    # csv_filename = "API_SP.POP.TOTL_DS2_en_csv_v2_6011311.csv" (Ancien nom, changé sans raison.)
    with zip_file.open(csv_filename) as csv_file:
        WB_population_data = pd.read_csv(csv_file, skiprows=4, encoding='ISO-8859-1')   
    return WB_population_data

def get_imf_indicators(url):
    """
    Retrieves the list of available indicators from the IMF API.
    Parameters:
    - url (str): The URL of the IMF API.
    Returns:
    DataFrame: The list of indicators in DataFrame format.
    """
    response = requests.get(url)
    response_body = json.loads(response.text)
    indicators = [
        {"id": key, **values} for key, values in response_body["indicators"].items()
    ]
    indicators_df = pd.DataFrame.from_records(indicators)
    return indicators_df

def get_imf_groups(group_type: str):
    """
    Retrieves groups of a specified type from the IMF API.
    Parameters:
    - group_type (str): The type of group (e.g., "countries" or "departments").
    Returns:
    DataFrame: The list of groups in DataFrame format.
    """
    response = requests.get(
        url=f"https://www.imf.org/external/datamapper/api/v1/{group_type}"
    )
    response_body = json.loads(response.text)
    results = [
        {"id": key, **values} for key, values in response_body[group_type].items()
    ]
    df = pd.DataFrame.from_records(results)
    return df

def get_imf_indicator_data(
    indicator_id: str, 
    group_ids: list[str] = [], 
    years: list[str] = []
):
    """
    Retrieves data for a specified indicator, groups, and years from the IMF API.
    Parameters:
    - indicator_id (str): The identifier of the indicator.
    - group_ids (list[str]): The list of group identifiers.
    - years (list[str]): The list of years.
    Returns:
    DataFrame: The indicator data in DataFrame format.
    """
    indicator_url = "https://www.imf.org/external/datamapper/api/v1"
    groups_url_path = "/".join(group_ids)
    years_query_param = "?periods=" + ",".join(years)
    response = requests.get(
        url=f"{indicator_url}/{indicator_id}/{groups_url_path}{years_query_param}"
    )
    response_body = json.loads(response.text)
    response_values = response_body.get("values")
    if not response_values:
        return pd.DataFrame()
    indicator_df = pd.DataFrame.from_records(
        response_body["values"][indicator_id]
    ).sort_index()
    return indicator_df

def get_imf_raw_data(url):
    """
    Retrieves raw IMF data for the 'Central Government Debt' indicator, selected countries, and the years 1990 to 2021.
    Parameters:
    - url (str): The URL of the IMF API.
    Returns:
    DataFrame: The raw IMF data for the specified parameters in DataFrame format.
    """
    indicators = get_imf_indicators(url)
    indicator = indicators.loc[indicators.label == 'Central Government Debt'].iloc[0]
    countries = get_imf_groups('countries')
    selected_countries = countries.loc[countries.label.isin([])]
    selected_years = [str(year) for year in range(1990, 2022)]
    IMF_raw_data = get_imf_indicator_data(indicator.id, selected_countries.id, selected_years)
    IMF_raw_data.index = IMF_raw_data.index.astype(int)
    return IMF_raw_data