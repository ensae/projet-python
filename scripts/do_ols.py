import statsmodels.api as sm
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

def reg_maison(data, x_col, y_col, method='HC3'):
    """
    Performs a simple regression of y_col on x_col using the specified method.
    Parameters:
    - data (DataFrame): The input data.
    - x_col (str): The independent variable.
    - y_col (str): The dependent variable.
    - method (str): The method for estimating the covariance matrix (default is 'HC3').
    Returns:
    None: Prints regression results and plots the data points with the regression line.
    """
    data = data.dropna()
    data = sm.add_constant(data)
    X = data[['const', x_col]]
    y = data[y_col]
    model = sm.OLS(y, X).fit(cov_type=method)
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5), gridspec_kw={'width_ratios': [6, 5]})
    summary_text = str(model.summary())
    ax1.text(0, 0.5, summary_text, va='center', ha='left', fontsize=10, family='monospace')
    ax1.axis('off') 
    sns.scatterplot(x=x_col, y=y_col, data=data, ax=ax2, color='skyblue')
    sns.regplot(x=x_col, y=y_col, data=data, scatter=False, ax=ax2, color = 'sandybrown')
    ax2.set_title(f'Regression of {y_col} on {x_col}')
    plt.tight_layout()
    plt.show()