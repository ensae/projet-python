import plotly.express as px
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import KMeans

def plot_debt_data(data, col):
    """
    Plots the yearly and cumulative missing values for a specific column.
    Parameters:
    - data (DataFrame): The input dataframe containing the data.
    - col (str): The column for which missing values are plotted.
    """
    missing_values_per_year = data.groupby('year')[col].apply(lambda x: x.isnull().sum())
    missing_values = data[col].isnull().sum()
    cumulative_missing_values = missing_values_per_year.cumsum()
    cumulative_missing_percentage = (cumulative_missing_values / missing_values) * 100
    cumulative_missing_percentage_int = cumulative_missing_percentage.round().astype(int)
    fig, ax1 = plt.subplots(figsize=(12, 6))
    bars = ax1.bar(missing_values_per_year.index, missing_values_per_year.values, color='skyblue', label='Yearly Missing Values')
    for bar in bars:
        yval = bar.get_height()
        ax1.text(bar.get_x() + bar.get_width()/2, yval, int(yval), va='bottom', ha='center')
    ax1.set_xlabel('year')
    ax1.set_ylabel('Number of Missing Values per Year', color='skyblue')
    ax1.set_title('Yearly and Cumulative Missing in CG_debt')
    ax1.set_xticks(missing_values_per_year.index)
    ax1.set_xticklabels(missing_values_per_year.index, rotation=90)
    ax2 = ax1.twinx()
    ax2.plot(cumulative_missing_values.index, cumulative_missing_percentage_int.values, color='orange', marker='o', label='Cumulative Missing Values (%)')
    ax2.set_ylabel('Cumulative Missing Values since 1990 in %', color='orange')
    plt.show()

def plot_mean_evolution(data, x_col, y_col, std_values=[0.5, 1], colors=['deepskyblue', 'lightblue']):
    """
    Plots the evolution of mean HDI with specified standard deviations.
    Parameters:
    - data (DataFrame): Input DataFrame containing HDI data.
    - x_col (str): Column name for the dependant variable.
    - y_col (str): Column name for the data variable.
    - std_values (list): List of standard deviation values to plot.
    - colors (list): List of colors for each standard deviation line.
    Returns:
    - None: Displays the plot.
    """
    mean_hdi_by_year = data.groupby(x_col)[y_col].mean()
    std_dev_hdi_by_year = data.groupby(x_col)[y_col].std()
    plt.figure(figsize=(12, 6))
    for std, color in zip(std_values, colors):
        upper_bound = mean_hdi_by_year + std * std_dev_hdi_by_year
        lower_bound = mean_hdi_by_year - std * std_dev_hdi_by_year
        plt.plot(upper_bound.index, upper_bound, label=f'+{std} sd', color=color)
        plt.plot(lower_bound.index, lower_bound, label=f'-{std} sd', color=color)
    plt.plot(mean_hdi_by_year.index, mean_hdi_by_year, label=f'Mean {y_col}', color='darkblue')
    plt.title(f'Evolution of Mean {y_col} with {", ".join(map(str, std_values))} Standard Deviations')
    plt.xlabel(x_col)
    plt.ylabel(f'Mean {y_col}')
    plt.legend()
    plt.show()

def plot_quartile_evolution(data, x_col, y_col, n_quartiles=4, figsize=(15, 10)):
    """
    Plots the evolution of HDI for each quartile.
    Parameters:
    - data (DataFrame): Input DataFrame containing HDI data.
    - x_col (str): Column name for the dependant variable.
    - y_col (str): Column name for the data variable.
    - n_quartiles (int): Number of quartiles.
    - figsize (tuple): Size of the figure.
    Returns:
    - None: Displays the plot.
    """
    hdi_1990 = data[data[x_col] == 1990]
    first_quartile_hdi_1990 = hdi_1990[y_col].quantile(0.25)
    second_quartile_hdi_1990 = hdi_1990[y_col].quantile(0.5)
    third_quartile_hdi_1990 = hdi_1990[y_col].quantile(0.75)
    fourth_quartile_hdi_1990 = hdi_1990[y_col].quantile(1.0)
    countries_first_quartile = data[(data[x_col] == 1990) & (data[y_col] <= first_quartile_hdi_1990)]
    countries_second_quartile = data[(data[x_col] == 1990) & 
                                              (data[y_col] > first_quartile_hdi_1990) & 
                                              (data[y_col] <= second_quartile_hdi_1990)]
    countries_third_quartile = data[(data[x_col] == 1990) & 
                                             (data[y_col] > second_quartile_hdi_1990) & 
                                             (data[y_col] <= third_quartile_hdi_1990)]
    countries_fourth_quartile = data[(data[x_col] == 1990) & 
                                              (data[y_col] > third_quartile_hdi_1990) & 
                                              (data[y_col] <= fourth_quartile_hdi_1990)]
    # Graphe pour chaque quartile
    fig, axs = plt.subplots(2, 2, figsize=figsize)
    for country_code in countries_first_quartile['country_code'].unique():
        country_data = data[data['country_code'] == country_code]
        axs[0, 0].plot(country_data[x_col], country_data[y_col], label=f'{country_code} (1st Quartile)')
    for country_code in countries_second_quartile['country_code'].unique():
        country_data = data[data['country_code'] == country_code]
        axs[0, 1].plot(country_data[x_col], country_data[y_col], label=f'{country_code} (2nd Quartile)')
    for country_code in countries_third_quartile['country_code'].unique():
        country_data = data[data['country_code'] == country_code]
        axs[1, 0].plot(country_data[x_col], country_data[y_col], label=f'{country_code} (3rd Quartile)')
    for country_code in countries_fourth_quartile['country_code'].unique():
        country_data = data[data['country_code'] == country_code]
        axs[1, 1].plot(country_data[x_col], country_data[y_col], label=f'{country_code} (4th Quartile)')
    # Finalisation
    axs[0, 0].set_title('1st Quartile')
    axs[0, 1].set_title('2nd Quartile')
    axs[1, 0].set_title('3rd Quartile')
    axs[1, 1].set_title('4th Quartile')
    for ax in axs.flat:
        ax.set(xlabel=x_col.capitalize(), ylabel=y_col)
    plt.tight_layout()
    plt.show()

def plot_clusters(traj, num_clusters, x_col, y_col, size):
    """
    Plots the average evolution of HDI for each cluster over a specified number of years.
    Parameters:
    - traj (DataFrame): The input dataframe containing trajectory data.
    - num_clusters (int): The number of clusters for KMeans clustering.
    - x_col (str): The column for the x-axis label.
    - y_col (str): The column for the y-axis label.
    - size (int): The number of years for the trajectory.
    Notes:
    - Assumes the trajectory dataframe has columns named 'delta_0' to 'delta_{size-1}' for each year.
    """
    traj_noNan = traj.dropna()
    traj_noNan = traj_noNan.reset_index(drop=True).copy() 
    data_for_clustering = traj_noNan[[f'delta_{i}' for i in range(size)]]
    kmeans = KMeans(n_clusters=num_clusters, n_init=10, random_state=42)
    traj_noNan['cluster'] = kmeans.fit_predict(data_for_clustering)
    cluster_centers = kmeans.cluster_centers_
    plt.figure(figsize=(12, 6))
    for i in range(num_clusters):
        plt.plot(range(size), cluster_centers[i], label=f'Cluster {i+1}', marker='o')
    plt.xticks(range(size), range(size))
    plt.xlabel(x_col.capitalize())
    plt.ylabel(f'{y_col} value')
    plt.title(f'Average {size}-year evolution of HDI in each cluster')
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()

def world_map(dataframe, y_col, data_name, width=900, height=500):
    """
    Creates an animated world map showing the distribution of a specific data column across countries and years.
    Parameters:
    - dataframe (DataFrame): The input dataframe containing country-level data.
    - y_col (str): The column to be visualized on the map (e.g., 'HDI' or 'CG_debt').
    - data_name (str): A label for the data being visualized (e.g., 'IDH' or 'Dette publique').
    - width (int): The width of the map figure.
    - height (int): The height of the map figure.
    Notes:
    - Assumes the dataframe has columns 'country_code', 'year', and the specified y_col.
    """
    min_value = dataframe[y_col].min()
    max_value = dataframe[y_col].max()
    # Création d'une carte du monde avec Plotly
    fig = px.choropleth(dataframe, 
                        locations='country_code',
                        color=y_col,
                        hover_name='country_code',
                        color_continuous_scale="turbo",
                        projection='natural earth',
                        animation_frame='year',
                        range_color=(min_value, max_value))
    fig.update_coloraxes(colorbar_title_text='')
    # Mise en page des sous-plots pour les barres d'histogramme
    fig.update_layout(
        xaxis2=dict(domain=[0, 0.45], anchor='y2'),
        xaxis3=dict(domain=[0.55, 1], anchor='y3'),
        yaxis2=dict(domain=[0, 1], anchor='x2'),
        yaxis3=dict(domain=[0, 1], anchor='x3'),
        title_text= f'{data_name} par pays, 1990-2021',
    )
    # Ajout d'un slider pour choisir l'année
    slider = go.layout.Slider(
        currentvalue=dict(prefix="Année: "),
        font=dict(size=16),
        len=0.9,
        pad=dict(t=50, b=10),
        steps=[
            {"args": [f"slider{i}.value", {"duration": 400, "frame": {"duration": 400, "redraw": True}, "mode": "immediate"}],
             "label": str(i),
             "method": "animate",
            } for i in range(1990, 2022)
        ],
    )
    fig.update_layout(
    sliders=[
        {
            "steps": [
                {"args": [[f"{year}"], {"frame": {"duration": 500, "redraw": True}, "mode": "immediate", "transition": {"duration": 500}}], "label": f"{year}", "method": "animate"} 
                for year in sorted(dataframe['year'].unique())
            ],
            "active": 0,
            "yanchor": "top",
            "xanchor": "left",
            "transition": {"duration": 300, "easing": "cubic-in-out"},
        }
    ],
    updatemenus=[{"type": "buttons", "showactive": False, "buttons": [{"label": "Play", "method": "animate", "args": [None, {"frame": {"duration": 500, "redraw": True}, "fromcurrent": True, "transition": {"duration": 300, "easing": "quadratic-in-out"}}]}]},
                 {"type": "buttons", "showactive": False, "buttons": [{"label": "Quick", "method": "animate", "args": [None, {"frame": {"duration": 0, "redraw": True}, "transition": {"duration": 0}}]}]}]
)
    fig.update_layout(
        geo=dict(
            showcoastlines=True,
            coastlinecolor="Black",
            showland=True,
            landcolor="white",
    ),
    width=width,
    height=height
)
    fig.show()