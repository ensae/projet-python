import pandas as pd

def clean_ISO(rows):
    """
    Cleans and organizes ISO country code data extracted from HTML rows.
    Parameters:
    - rows: BeautifulSoup ResultSet containing rows of ISO country codes.
    Returns:
    DataFrame: The cleaned and formatted ISO country code data in DataFrame format.
    """
    dict_iso_country_codes = dict()
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        if len(cols) > 0 : 
            dict_iso_country_codes[cols[0]] = cols[1:]
    ISO_country_codes = pd.DataFrame.from_dict(dict_iso_country_codes,orient='index')
    for row in rows:
        cols = row.find_all('th')
        if len(cols) > 0 : 
            cols = [ele.get_text(separator=' ').strip().title() for ele in cols]
            columns_ISO_country_codes = cols
    ISO_country_codes.columns = columns_ISO_country_codes[1:]
    ISO_country_codes = ISO_country_codes[ISO_country_codes['Indépendant'] == 'Oui']
    columns_to_drop = [1, 2, 4, 5, 6]
    ISO_country_codes = ISO_country_codes.drop(ISO_country_codes.columns[columns_to_drop], axis=1)
    ISO_country_codes = ISO_country_codes.rename(columns={'Alpha-3': 'country_code', 'Nom Français': 'country_name'})
    ISO_country_codes = ISO_country_codes.reset_index(drop=True)
    return ISO_country_codes

def clean_pop(data, ISO_country_codes):
    """
    Cleans and organizes World Bank population data.
    Parameters:
    - data (DataFrame): Raw World Bank population data.
    - ISO_country_codes (DataFrame): Cleaned ISO country code data.
    Returns:
    DataFrame: Cleaned and formatted population data in DataFrame format.
    """
    # Suppression des éléments inutiles
    columns_to_drop = ['Country Name','Indicator Name', 'Indicator Code', 'Unnamed: 67']
    WB_population_data = data.drop(columns=columns_to_drop, axis=1)
    columns_to_drop = [str(year) for year in range(1960, 1990)]
    WB_population_data = WB_population_data.drop(columns=columns_to_drop)
    # Suppression des entités ne correspondant pas à des pays
    WB_population_data = WB_population_data.rename(columns={'Country Code': 'country_code'})
    population_data = WB_population_data.merge(ISO_country_codes, on='country_code', how='right')
    # Tidy
    population_data = pd.melt(population_data, id_vars=['country_code'], var_name='year', value_name='population')
    population_data['year'] = pd.to_numeric(population_data['year'], errors='coerce').astype('Int64')
    population_data['population'] = pd.to_numeric(population_data['population'], errors='coerce')
    population_data = population_data.dropna(subset=['year'])
    population_data = population_data.sort_values(by=['country_code', 'year'])
    population_data = population_data.reset_index(drop=True)
    return population_data

def clean_hdi(raw_data):
    """
    Cleans and organizes HDI data by removing unnecessary columns, 
    filtering out entries with 'ZZ' country codes, and renaming columns.
    Parameters:
    - raw_data (DataFrame): Raw HDI data.
    Returns:
    DataFrame: Cleaned and formatted HDI data in DataFrame format.
    """
    # Supprimer les éléments superflus
    columns_to_drop = raw_data.columns[1:3]
    clean_data = raw_data.drop(columns=columns_to_drop, axis=1)
    clean_data = clean_data[~clean_data['country'].str.startswith('ZZ')]
    # Séparer les codes ISO et les nomes des pays
    clean_data[['country_code', 'country_name']] = clean_data['country'].str.split('-', n=1, expand=True)
    clean_data['country_code'] = clean_data['country_code'].str.strip()
    clean_data['country_name'] = clean_data['country_name'].str.strip()
    # Tidy
    clean_data = clean_data[['country_code', 'year', 'value']]
    clean_data = clean_data.rename(columns={'value': 'HDI'})
    clean_data['year'] = pd.to_numeric(clean_data['year'], errors='coerce').astype('Int64')
    clean_data['HDI'] = pd.to_numeric(clean_data['HDI'], errors='coerce')
    return clean_data 

def clean_IMF(raw_data):
    """
    Cleans and organizes IMF data by resetting the index and melting the DataFrame.
    Parameters:
    - raw_data (DataFrame): Raw IMF data.
    Returns:
    DataFrame: Cleaned and formatted IMF data in DataFrame format.
    """
    df_reset = raw_data.reset_index()
    IMF_debt_data = pd.melt(df_reset, id_vars='index')
    IMF_debt_data.columns = ['year', 'country_code', 'CG_debt']
    IMF_debt_data = IMF_debt_data[['country_code', 'year', 'CG_debt']]
    return IMF_debt_data